# workshop-manager

> A Vue.js project

## Build Setup

### App configuration

- Provide back-end configuration in `src/config.js`
- Provide school fields in `src/components/workshop/fields-options.js`

```bash
# install dependencies
yarn

# serve with hot reload at localhost:8080
yarn run serve

# or
vue serve

# build for production with minification
yarn build

# build for production
yarn run build
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
