import Vue from 'vue'
import VueRouter from 'vue-router'

import Admin from '@/components/admin'
import UserSelfReport from '@/components/userSelfReport'
import WorkshopSurvey from '@/components/workshop/WorkshopSurvey'

Vue.use(VueRouter)

var routes = [
  {
    path: '/',
    name: 'home',
    component: WorkshopSurvey,
  },
  {
    path: '/administration',
    name: 'admin',
    component: Admin,
  },
  {
    path: '/mesRapports',
    name: 'rapports',
    component: UserSelfReport,
  },
]

export default new VueRouter({
  mode: 'history',
  routes,
})
