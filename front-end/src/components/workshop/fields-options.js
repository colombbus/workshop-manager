// export const allTapEstablishments = process.env.VUE_APP_SCHOOLS.split(',')
// export const allHighSchoolEstablishments = process.env.VUE_APP_COLLEGES.split(',')
export const allDays = [
  { label: 'mardi 15h00 - 16h30', value: 2 },
  { label: 'vendredi 15h00 - 16h30', value: 5 },
]

export const allAppreciationRatings = ['mauvaise', 'normale', 'bonne']

export const allHadBugsOptions = ['oui', 'non']
