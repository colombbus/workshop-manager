import '@fortawesome/fontawesome-free/css/all.min.css'

import '@/styles/main.sass'
import router from '@/router'
import Application from '@/Application'

import Vue from 'vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(Application),
  router,
}).$mount('#app')
