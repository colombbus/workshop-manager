<?php
require_once '../controllers/ReportController.php';
require_once '../controllers/AnimatorController.php';
require_once '../controllers/AdminController.php';
require_once '../controllers/establishmentController.php';

require_once '../services/Application.php';
require_once '../utils/Logger.php';

// define('BASE_PATH', '/declick-workshops-survey/back-end');
define('BASE_PATH', '/workshop-manager/back-end');

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type');

function handleRequest () {
  $method = $_SERVER['REQUEST_METHOD'];
  $path = substr($_SERVER['REQUEST_URI'], strlen(BASE_PATH));
 
  $rawBody = file_get_contents('php://input');
  if (!$rawBody) {
    $rawBody = '{}';
  }
  $body = json_decode($rawBody);
  if ($method === 'OPTIONS') {
    exit;
  }


  // var_dump($method, $path);



  if ($method === 'GET' && $path === '/install') {
    Application::install();
  } else if ($method === 'GET' && $path === '/reports.csv') {
    ReportController::download($body);
  } else if ($method === 'POST' && $path === '/reports') {
    ReportController::create($body);
  } else if( ($method === 'POST' && $path === '/login')){
    AdminController::authentification($body);
  } else if ( $method === 'GET' && $path === '/users' ) {
    AnimatorController::getAllAnimators();
  } else if ( $method === 'POST' && $path === '/newUser' ) {
    AnimatorController::createAnimator($body);
  }  else if ( $method === 'POST' && $path === '/removeUser' ) {
    AnimatorController::removeUser($body);
  } else if ( $method === 'POST' && $path === '/sendCode' ) {
    AnimatorController::sendCode($body);
  } else if ( $method === 'POST' && $path === '/csvAddUser' ) {
    AnimatorController::csvAddUser($_FILES);
  } else if ( $method === 'GET' && $path === '/allReports' ) {
    ReportController::showAll();
  } else if ( $method === 'POST' && $path === '/myRepport' ) {
    ReportController::myReports($body);
  } else if ( $method === 'POST' && $path === '/addEstablishment' ) {
    establishmentController::newEstablishment($body);
  } else if ( $method === 'GET' && $path === '/allEstablishments' ) {
    establishmentController::showAll();
  } else if ( $method === 'POST' && $path === '/removeEstablishments' ) {
    establishmentController::removeEstablishments($body);
  }else if ($method === 'POST' && $path === "/updateMail"){
    AnimatorController::updateMail($body);
  } else {
    http_response_code(404);
  }
}

function handleError ($id, $message, $file, $line) {
  throw new ErrorException($message, $id, 0, $file, $line);
}
set_error_handler('handleError');

function output ($data) {
  header('Content-type: application/json');
  echo json_encode($data);
}

try {
  handleRequest();
} catch (BadRequestException $exception) {
  http_response_code(400);
  output([
    'error' => 'bad-request-error',
    'description' => $exception->getMessage()
  ]);
} catch (Exception $exception) {
  Logger::error($exception);
  http_response_code(500);
}


