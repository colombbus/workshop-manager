<?php
require_once '../models/Animator.php';
require_once '../models/Report.php';
require_once '../Database.php';


class Application {
  public static function install () {
    Animator::createTable();
    Report::createTable();
  }
}
