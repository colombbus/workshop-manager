<?php 
require_once "../Database.php";

class establishmentController {

  public static function newEstablishment($establishment){
    $test = Database::process("INSERT INTO workshops_establishments ($establishment->type) VALUES (:establishmentName)",['establishmentName'=>$establishment->name]);
    if($test){
      echo true;
    } else {
      echo false;
    }
  }

  public static function showAll(){
    $test = Database::fetch("SELECT * FROM workshops_establishments");
    echo json_encode($test);
  }

  public static function removeEstablishments($establishment){
    $test = Database::process("DELETE FROM workshops_establishments WHERE id = :id",['id' => $establishment->id]);
    echo json_encode($test);    
  }
}