<?php
require_once '../models/Animator.php';
require_once '../Database.php';

class AnimatorController
{

    public static function getAllAnimators()
    {
        $test =  Database::fetch('SELECT * FROM workshops_animators;');
        echo json_encode($test);
    }

    public static function createAnimator($user)
    {

        if (Self::checkUserNameDisp($user->name)) {
            $userCode = Self::createCode();

            if ($userCode) {

                $result = Database::process('INSERT INTO workshops_animators (`name`,code,email) VALUES(:username,:code,:email)', ['username' => $user->name, 'code' => $userCode, 'email' => $user->email]);

                if (Self::sendCodeOnRegister(['name' => $user->name, 'email' => $user->email, 'code' => $userCode])) {
                    echo json_encode(["success" => ["name" => $user->name, "code" => $userCode, "email" => $user->email]]);
                } else {
                    echo json_encode(["error" => "quelque chose a du mal se passer vérifier que l'utisateur est enregistrer et renvoyer le code"]);
                }
            }
        } else {
            echo json_encode(["error" => "nom déja existant"]);
        }
    }

    static function createCode()
    {
        $char = "abcdefghijkmnopqrstuvwxyz1234567890";
        $newCodeAnim = '';
        for ($i = 0; $i < 5; $i++) {
            $newCodeAnim .= $char[rand(0, strlen($char) - 1)];
        }
        if (Self::checkCodeExist($newCodeAnim)) {

            return $newCodeAnim;
        } else {
            Self::createCode();
        }
    }

    static function checkCodeExist($test)
    {
        $data =  Database::fetch(
            "SELECT * FROM workshops_animators WHERE code = :test;",
            ['test' => $test]
        );
        if (empty($data)) {
            return true;
        } else {
            return false;
        }
    }

    static function checkUserNameDisp($test)
    {
        $data = Database::fetch(
            "SELECT * FROM workshops_animators WHERE `name` = :test;",
            ['test' => $test]
        );
        if (empty($data)) {
            return true;
        } else {
            return false;
        }
    }

    public static function removeUser($user)
    {
        $result = Database::process('DELETE FROM workshops_animators WHERE `name` = :username', ['username' => $user->name]);
    }

    public static function sendCode($user)
    {

        Self::checkUserMail($user);

        if (is_array($user)) {
            $username = $user["name"];
            $code = $user["code"];

            $mail = $user["email"];
        } else {
            $username = $user->name;
            $code = $user->code;

            $mail = $user->email;
        }

        $headers = 'From: Colombbus <declick@colombbus.org>' . "\r\n" .
            'Content-Type: text/html; charset=utf-8' . "\r\n";

        $content = "<!DOCTYPE html>
        <html>
          <head>
            <meta charset=\"utf-8\">
            <style>
        html, body {
          width: 100%;
          height: 100%;
          margin: 0;
          padding: 0;
        }
        body {
          font-family: \"Lucida Grande\", \"Lucida Sans Unicode\", Tahoma, sans-serif;
          font-size: 14px;
        }
        #container {
          width: 600px;
          margin: 10px auto;
        }
        table {
          border: 1px solid grey;
          border-collapse: collapse;
        }
        th, td {
          padding: 5px;
          border-top: 1px solid grey;
        }
        th {
          margin-right: 20px;
          text-align: right;
          background-color: #bbe5f1;
        }
            </style>
          </head>
          <body>
            <div id='container'>";

        $content .= "<p>Voici votre code animateur declick : $code </p>";

        $content .= "</body></html>";

        $subject = "code animateur";

        if (mail(
            $mail,
            $subject,
            $content,
            $headers
        )) {
            echo json_encode(["success" => true]);
        } else {
            echo json_encode(["success" => false]);
        }
    }
    public static function sendCodeOnRegister($user)
    {

        if (is_array($user)) {
            $username = $user["name"];
            $code = $user["code"];

            $mail = $user["email"];
        } else {
            $username = $user->name;
            $code = $user->code;

            $mail = $user->email;
        }

        $headers = 'From: Colombbus <declick@colombbus.org>' . "\r\n" .
            'Content-Type: text/html; charset=utf-8' . "\r\n";

        $content = "<!DOCTYPE html>
        <html>
          <head>
            <meta charset=\"utf-8\">
            <style>
        html, body {
          width: 100%;
          height: 100%;
          margin: 0;
          padding: 0;
        }
        body {
          font-family: \"Lucida Grande\", \"Lucida Sans Unicode\", Tahoma, sans-serif;
          font-size: 14px;
        }
        #container {
          width: 600px;
          margin: 10px auto;
        }
        table {
          border: 1px solid grey;
          border-collapse: collapse;
        }
        th, td {
          padding: 5px;
          border-top: 1px solid grey;
        }
        th {
          margin-right: 20px;
          text-align: right;
          background-color: #bbe5f1;
        }
            </style>
          </head>
          <body>
            <div id='container'>";

        $content .= "<p>Bonjour, </p>";
        $content .= "<p>Voici votre code animateur Colombbus-Declick : $code </p>";
        $content .= "<p>A la fin de chaque atelier vous devez déclarer votre présence.</p>";
        $content .= "<p>Accédez à https://ateliers-declick.colombbus.org/</p>";
        $content .= "<p>Complétez le formulaire en vous identifiant avec votre code animateur.</p>";
        $content .= "<p>ATTENTION : cette déclaration est primordiale pour la comptabilité interne et votre fiche de paie.</p>";
        $content .= "</body></html>";

        $subject = "Colombbus TAP code animateur pour déclaration présence atelier";

        if (mail(
            $mail,
            $subject,
            $content,
            $headers
        )) {
            return true;
        } else {
            return false;
        }
    }
    public static function csvAddUser($csv)
    {
        // $d = fgetcsv(fopen($data['file']['tmp_name'],'r'),filesize($data['file']['tmp_name']),' ');
        // var_dump($d);
        $row = 0;
        $name = '';
        $email = '';
        $responce = ["error" => '', "success" => ''];
        if (($handle = fopen($csv['file']['tmp_name'], "r")) !== FALSE) {
            while (($data = fgetcsv($handle, filesize($csv['file']['tmp_name']), ",")) !== FALSE) {
                $num = count($data);
                $row++;
                for ($c = 0; $c < $num; $c++) {
                    if ($c == 0) {
                        $name = $data[$c];
                    }
                    if ($c == 1) {
                        $email = $data[$c];
                        if (Self::createAnimatorCSV($name, $email)) {
                            $responce['success'] .= "$name $email;";
                        } else {
                            $responce['error'] .= "$name $email;";
                        }
                    }
                }
            }
            fclose($handle);
            echo json_encode($responce);
        }
    }

    static function createAnimatorCSV($name, $email)
    {

        if (Self::checkUserNameDisp($name)) {
            $userCode = Self::createCode();

            if ($userCode) {

                $result = Database::process('INSERT INTO workshops_animators (`name`,code,email) VALUES(:username,:code,:email)', ['username' => $name, 'code' => $userCode, 'email' => $email]);

                if (Self::sendCodeOnRegister(['name' => $name, 'email' => $email, 'code' => $userCode])) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }


    public static function checkUserMail($user)
    {



        $result = Database::fetch(
            "SELECT * FROM workshops_animators WHERE `name` = :userName;",
            ["userName" => $user->name]
        );


        if ($result[0]->email === null) {
            $update = Database::process("UPDATE `workshops_animators` SET `email`='$user->email' WHERE `name`= '$user->name';");
        }
    }


    public static function updateMail($user){
        $update = Database::process("UPDATE `workshops_animators` SET `email`='$user->email' WHERE `name`= '$user->name';");
    }
}
