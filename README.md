# workshop-manager

## configuration

- front-end/.env
- front-end/src/config.js
- back-end/Database.php

## deploy

- yarn build
- mv dist ../dist
- cd ../
- mv dist/index.html index.html
- tar cvzf workshop-manager.tar.gz back-end/ dist/ index.html 
